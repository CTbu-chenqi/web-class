package com.lc.demo.aspect;

import com.lc.demo.core.RUtil;
import com.lc.demo.exception.REnum;
import com.lc.demo.exception.RException;
import com.lc.demo.vo.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandle {
    @ExceptionHandler(value = Exception.class)
    public R handle(Exception e){
        if(e instanceof RException){
            RException rException = (RException) e;
            return RUtil.error(rException.getCode(),rException.getMessage());
        }

        return RUtil.error(REnum.UNKNOW_ERR);
    }
}

