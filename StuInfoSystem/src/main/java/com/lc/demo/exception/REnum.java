package com.lc.demo.exception;

/***
 * 关于异常的枚举类
 */
public enum REnum {
    UNKNOW_ERR(-999,"未知错误"),
    COMMON_ERR(-10,"一般性错误"),
    LOGIN_ERR(-2,"不正确的用户名或者密码"),
    SUCCESS(1,"成功");


    private final Integer code;

    private final String msg;

    REnum(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMsg(){
        return this.msg;
    }
}

